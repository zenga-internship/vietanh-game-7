﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using Unity.Burst;

public class WreckSceneManager : MonoBehaviour
{
    [SerializeField] GameObject cube;
    [SerializeField] int x = 10;
    [SerializeField] int y = 100;
    public TMP_InputField data;
    int count;
    // Start is called before the first frame update
    void Start()
    {
        data.text = "1";
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                Instantiate(cube, new Vector3(i - 25, j + 10, 0), Quaternion.identity);
            }
        }
    }
    void Update()
    {
        count = int.Parse(data.text);
    }
    public void Next()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void Previous()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void Click()
    {
        for (int i = 0; i < count; i++)
        {
            Instantiate(cube, new Vector3(0, 10 + i, 0), Quaternion.identity);
        }
    }
}
