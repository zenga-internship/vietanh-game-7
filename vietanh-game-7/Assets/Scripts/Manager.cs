﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
public class Manager : MonoBehaviour
{
    // Start is called before the first frame update
    EntityManager manager;
    [SerializeField] GameObject cube;
    [SerializeField] int ObjectPerClick;
    [SerializeField] TMP_InputField max;
    public TMP_Text text;
    Vector3 spawnPos;
    static int count;
    int max_num;

    void Start(){
        
    }
    void Update()
    {
        //max_num = Convert.ToInt32(max.text);
        text.text = count.ToString();
    }
    public void Creat()
    {
        for (int i = 0; i < ObjectPerClick; i++)
        {
            spawnPos.y = 100;
            spawnPos.x = UnityEngine.Random.Range(-30, 30);
            spawnPos.z = UnityEngine.Random.Range(-50, 50);
            Instantiate(cube, spawnPos, Quaternion.identity);
            count++;
        }

    }
    public static void AddPoint(int num)
    {
        count += num;
    }
    public void Next()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void Previous()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
