﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Collections;
public class NewBehaviourScript : MonoBehaviour
{
    public GameObject cube;

    // Start is called before the first frame update
    void Start()
    {
        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        NativeArray<Entity> entityArray = new NativeArray<Entity>(1000, Allocator.Temp);
        for (int i = 0; i < 1000; i++)
        {
            entityManager.Instantiate(cube, entityArray);
        }
        entityArray.Dispose();
    }
}
