﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class EntitySpawnerSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if (true) Entities.ForEach((PrefabEntityComponent prefabEntityComponent) =>
        {
            Entity spawnedEntity = EntityManager.Instantiate(prefabEntityComponent.prefabEntity);
            EntityManager.SetComponentData(spawnedEntity, new Translation { Value = new float3(0, 10, 0) });
        });
    }
}
