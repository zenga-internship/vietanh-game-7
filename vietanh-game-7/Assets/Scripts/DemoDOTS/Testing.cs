﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;
using Unity.Rendering;
using Unity.Jobs;

public class Testing : MonoBehaviour
{
    [SerializeField] GameObject cube;
    [SerializeField] Mesh mesh;
    [SerializeField] Material material;
    private void Start()
    {
        EntityManager entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        EntityArchetype entityArchetype = entityManager.CreateArchetype(typeof(Translation), typeof(RenderMesh), typeof(LocalToWorld), typeof(RenderBounds), typeof(MoverComponent),typeof(Rigidbody),typeof(BoxCollider));
        NativeArray<Entity> entityArray = new NativeArray<Entity>(10000, Allocator.Temp);
        entityManager.CreateEntity(entityArchetype, entityArray);
        //entityManager.Instantiate(cube,entityArray);
        for (int i = 0; i < entityArray.Length; i++)
        {
            Entity entity = entityArray[i];
            entityManager.SetSharedComponentData(entity, new RenderMesh { mesh = mesh, material = material });
            entityManager.SetComponentData(entity, new Translation { Value = new Vector3(Random.Range(-2.5f, 2.5f), Random.Range(-5f, 5f), Random.Range(-5f, 5f)) });
            entityManager.SetComponentData(entity, new MoverComponent { speed = 1 });
        }
        entityArray.Dispose();
    }
}
