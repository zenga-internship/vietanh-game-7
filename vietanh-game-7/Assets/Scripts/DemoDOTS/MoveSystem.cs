﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Collections;

public class MoveSystem : JobComponentSystem
{
    protected override JobHandle  OnUpdate(JobHandle inputDeps)
    {
        JobHandle jobHandle=  Entities.ForEach((ref Translation trans, ref MoverComponent moveSpeed) =>
        {
            if (trans.Value.y > 5 || trans.Value.y < -5) moveSpeed.speed = -moveSpeed.speed;
            trans.Value.y += moveSpeed.speed;
        }).Schedule(inputDeps);

        return jobHandle;
    }
}
