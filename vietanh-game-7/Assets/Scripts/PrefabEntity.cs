﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
public class PrefabEntity : MonoBehaviour, IConvertGameObjectToEntity
{
    public static Entity prefabEntity;
    public GameObject prefabGameObject;
    public void Convert(Entity entity, EntityManager dstmanager, GameObjectConversionSystem conversionSystem)
    {
        using (BlobAssetStore blobAssetStore = new BlobAssetStore())
        {
            Entity prefabEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefabGameObject, GameObjectConversionSettings.FromWorld(dstmanager.World, blobAssetStore));
        }
    }
}
